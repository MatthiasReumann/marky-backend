#!/usr/bin/env bash

# firstly run docker run -p 8080:8080 --name envoy <container-id>

docker start envoy

cd ../server
go build ./*.go
./main

#cd ../js-client/
#npm run serve