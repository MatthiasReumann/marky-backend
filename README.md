# [Marky🐶 Collaborative Markdown Editing](https://www.markymd.io) 

Marky is an Open-Source (eventually decentralized) Collaborative Privacy Preserving Markdown Editor.

# Features
* Create and share Markdown Documents (via share links)
* Render Markdown to HTML (use your own templates)
* Random User Names based on Adjectives and Emojis (discover your favourites)
* Insert Markdown via ToolButtons (Bold, Italic, Strikethrough, Table, List, Code, ...)
* Save Documents
* Print Documents
* List of Active Users
* No Login as we do not want to save personal data!


# Roadmap

* Import/Export to Github?
* Mark other user positions as text overlay 
* No Cookies
* Fully Peer to Peer
* Security
* iOS App to view (also contribute on?)
* Anonymous Documents through Zero Knowledge Proofs

# Issues
* Cursor moves down eventually in vue js frontend 

# Architecture

# Build 

## Frontend

For non-dockerised version see `Run`.  
Inside the `js-client` directory run:  
`docker build -t marky-frontend:v1 .`

## Server
## Envoy Proxy

Inside the `envoy` directory run:
`docker build -t envoy:v1 .`

In order to override the properties from `envoy.yaml` the build has to be run with the `no-cache` flag added.
    
## iOS

# Technical TO-DO

* Please better configs (etc for ports addresses -timeouts etc)
* Better deployments
* UpdateReadme

TODO document how to setup ios dev here

## Protobuf

### protoc

### protoc-gen-grpc-web

1. Download from https://github.com/grpc/grpc-web/releases/tag/1.0.7
2. Move it to a folder (e.g. one that is in your $PATH) (might be skipped):  
    `sudo mv ~/Downloads/protoc-gen-grpc-web-1.0.7-darwin-x86_64 
       /usr/local/bin/protoc-gen-grpc-web`
       
 3. Make the generator executable:  
   `chmod +x /usr/local/bin/protoc-gen-grpc-web`
   
### Compile Proto files for go
` protoc -I proto/ proto/*.proto --go_out=plugins=grpc:proto`
   
### Generate Client Stub for web grpc
Run in project root dir:  
`protoc --proto_path=proto --js_out=import_style=commonjs,binary:js-client/src/ --grpc-web_out=import_style=commonjs,mode=grpcwebtext:js-client/src/ proto/user_connection.proto`

### Generate Client Stub for iOS
Run in project root dir:
`protoc --proto_path=proto \
     --swift_out=marky/marky \
     --swiftgrpc_out=Client=true,Server=false:marky/marky  proto/*.proto`
     
Refer to the `/js-client && /envoyproxy` directories for the respective documentation.
TODO: Add docker-compose for easier setup

### Server
Build the server inside the `/server` directory:  
 `docker build -t server:v1 .`
     
# Run

## Typical Port Mapping

| Service       | Listening Ports | Publishing Ports  |
| ------------- |-----------------| ------------------|
| gRPC Server   | -               | 1337              |
| Envoy         | 1337            | 8080, 9901 (admin interface)|
| vue Frontend  | 8080            | 8081              |
| iOS App       | 8080            |                   |

## Server
`docker run -p 1337:1337 server:v1`

## Envoy
Run from anywhere:   
`docker run -p 9901:9901 -p 8080:8080 envoy:v1`

## Frontend

### Locally 
Inside the `js-client` directory run:  
`npm run serve`

### Dcokerised
Run from anywhere:    
`docker run -p 80:80 marky-frontend:v1`

## iOS App   


# Test

# Architecture

# Contribute 