package main;

import (
	pb "gitlab.com/MatthiasReumann/marky-backend/proto"
	"google.golang.org/grpc"
	"log"
	"net"
)

const (
	port = ":1337"
)

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	grpcServer := grpc.NewServer()
	server := NewServer()
	pb.RegisterUserConnectionServer(grpcServer, &server)

	log.Printf("starting server at %s\n", port)
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %s", err)
	}
}
