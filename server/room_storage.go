package main

import (
	"errors"
	pb "gitlab.com/MatthiasReumann/marky-backend/proto"
)

const (
	MAX_SIZE = 10
)


type Storage interface {
	Add(roomID string, user *pb.User) error
	GetUsersByRoomID(roomID string) ([]*pb.User, error)
	ClearRoom(roomID string) error
	GetContentByRoomID(roomID string) (pb.Document, error)
	RemoveUserFromRoom(roomID, userID string) error
	UpdateDocumentNameByRoomID(roomID, docName string) error
}

type RoomStorage struct {
	rooms map[string][]*pb.User
}

func NewRoomStorage() *RoomStorage {
	return &RoomStorage{rooms: make(map[string][]*pb.User)}
}

func (r RoomStorage) Add(roomID string, user *pb.User) error {
	if len(r.rooms[roomID]) < MAX_SIZE {
		users := r.rooms[roomID]
		for _, currentUser := range users {
			if currentUser.UserId == user.UserId {
				return nil
			}
		}

		r.rooms[roomID] = append(r.rooms[roomID], user)

		return nil
	}

	return errors.New("room is full")
}

func (r RoomStorage) GetUsersByRoomID(roomID string) ([]*pb.User, error) {
	if !r.roomExists(roomID) {
		return nil, errors.New("room doesn't exist")
	}

	return r.rooms[roomID], nil
}

func (r RoomStorage) ClearRoom(roomID string) error {
	if !r.roomExists(roomID) {
		return errors.New("room doesn't exist")
	}

	r.rooms[roomID] = []*pb.User{}
	return nil
}

func (r RoomStorage) RemoveRoom(roomID string) {
	delete(r.rooms, roomID)
}

func (r RoomStorage) RemoveUserFromRoom(roomID, userID string) error {
	if !r.roomExists(roomID) {
		return errors.New("room does not exist")
	}

	idx, err := r.indexByUserID(roomID, userID)

	if err != nil {
		return err
	}

	r.rooms[roomID][idx] = r.rooms[roomID][len(r.rooms[roomID])-1]
	r.rooms[roomID] = r.rooms[roomID][:len(r.rooms[roomID])-1]

	return nil
}

func (r RoomStorage) roomExists(roomID string) bool {
	_, ok := r.rooms[roomID]
	return ok
}

func (r RoomStorage) indexByUserID(roomID, userID string) (int, error) {
	for i, el := range r.rooms[roomID] {
		if el.UserId == userID {
			return i, nil
		}
	}

	return -1, errors.New("user doesn't exist")
}