package main

import (
	"context"
	"github.com/google/uuid"
	pb "gitlab.com/MatthiasReumann/marky-backend/proto"
	"log"
	"time"
)

type server struct {
	pb.UnimplementedUserConnectionServer
	roomStorage     *RoomStorage
	documentStorage *DocumentStorage
}

func NewServer() server {
	return server{roomStorage: NewRoomStorage(),
		documentStorage: NewDocumentStorage()}
}

func (s *server) Connect(req *pb.UserConnectionRequest, srv pb.UserConnection_ConnectServer) error {
	log.Printf("Received: %v from user %v", req.GetDocumentId(), req.GetConnectingUser().GetUserId())

	docID := req.GetDocumentId()
	user := req.GetConnectingUser()

	connectedUsers, err := s.roomStorage.GetUsersByRoomID(docID)
	documentContent, err := s.documentStorage.GetContentByRoomID(docID)

	log.Printf(documentContent.GetName())

	if err != nil {
		log.Fatalf("Oasch! %s", err)
	}

	// add user
	s.roomStorage.Add(docID, user)

	for _, user := range connectedUsers {
		log.Printf(user.UserId)
	}

	for true {
		doc, err := s.documentStorage.GetContentByRoomID(docID)
		if err != nil {
			log.Println(err)
		}

		document := &pb.Document{DocumentId: docID, Name: doc.Name, Content: doc.Content}
		connectedUsers, err = s.roomStorage.GetUsersByRoomID(docID)

		if err != nil {
			log.Fatal(err)
		}

		srv.Send(&pb.UserConnectionResponse{ConnectedUsers: connectedUsers, InitialDocumentContent: document})

		time.Sleep(time.Duration(100) * time.Millisecond)
	}

	return nil
}

func (s *server) NewURL(ctx context.Context, req *pb.NewURLRequest) (*pb.NewURLResponse, error) {
	url := uuid.New().String()
	log.Printf("New URL generated: %s", url)
	return &pb.NewURLResponse{Url: url}, nil
}

func (s *server) UpdateDocumentName(ctx context.Context, req *pb.UpdateDocumentRequest) (*pb.UpdateDocumentResponse, error) {
	docName := req.GetName()
	docID := req.GetDocumentID()

	s.documentStorage.UpdateDocumentNameByRoomID(docID, docName)

	return &pb.UpdateDocumentResponse{}, nil
}

func (s *server) TextChanged(ctx context.Context, req *pb.TextChangedRequest) (*pb.TextChangedResponse, error) {
	roomID := req.GetRoomID()
	content := req.GetDocumentContent()

	s.documentStorage.SetContent(roomID, content)

	return &pb.TextChangedResponse{}, nil
}
