package main

import (
	"context"
	pb "gitlab.com/MatthiasReumann/marky-backend/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/test/bufconn"
	"io"
	"log"
	"net"
	"testing"
	"time"
	"fmt"
)


const (
	bufSize = 1024 * 1024
)


var lis *bufconn.Listener  // TODO is there ONE way to avoid global mutable state? 🤢🤮

func init() {
	lis = bufconn.Listen(bufSize)
	s := grpc.NewServer()
	storage := NewRoomStorage()

	pb.RegisterUserConnectionServer(s, &server{storage: storage})

	go func() {
		if err := s.Serve(lis); err != nil {
			log.Fatalf("Server exited with error: %v", err)
		}
	}()
}

func bufDialer(string, time.Duration) (net.Conn, error) {
	return lis.Dial()
}

func getBufnetConnection() (*grpc.ClientConn, context.Context) {
	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, "bufnet", grpc.WithDialer(bufDialer), grpc.WithInsecure())

	if err != nil {
		log.Fatalf("Failed to dial bufnet: %v", err)
	}

	return conn, ctx
}

func TestNewUrl(t *testing.T) {
	conn, ctx := getBufnetConnection()
	defer conn.Close()

	client := pb.NewUserConnectionClient(conn)

	urlResponse, err := client.NewURL(ctx, &pb.NewURLRequest{})

	if err != nil {
		t.Errorf("Could not get URL")
	}

	if (len(urlResponse.GetUrl()) != 36) {
		t.Errorf("UUID of invalid length %v", urlResponse.GetUrl())
	}
}

func receiveStream(stream *pb.UserConnection_ConnectClient, name string) {
	fmt.Printf("hansi: %v", name)

	for {
		content, err := (*stream).Recv()
		//content, err := stream.Recv()

		if err == io.EOF {
			break
		}

		if err != nil {
			log.Fatalf("Connect: %v", err)
		}

		fmt.Printf("%v: %v \n", name, content.GetInitialDocumentContent().GetContent())
	}
}

func TestConnect(t *testing.T) {
	conn, ctx := getBufnetConnection()
	defer conn.Close()

	ctx, cancel := context.WithTimeout(context.Background(), 5 * time.Second)
	defer cancel()

	client := pb.NewUserConnectionClient(conn)

	connectingUser1 := &pb.User{UserId: "1a", Name: "testy", Icon: "tennis"}
	connectingUser2 := &pb.User{UserId: "2b", Name: "testy", Icon: "trampolin"}
	documentId := "d-id-v4"

	streamUser1, err := client.Connect(ctx, &pb.UserConnectionRequest{ConnectingUser: connectingUser1, DocumentId: documentId})
	streamUser2, err := client.Connect(ctx, &pb.UserConnectionRequest{ConnectingUser: connectingUser2, DocumentId: documentId})

	go receiveStream(&streamUser1, connectingUser1.Icon)
	go receiveStream(&streamUser2, connectingUser2.Icon)

	//r2, _ := client.Connect(ctx, &pb.UserConnectionRequest{ConnectingUser: connectingUser2, DocumentId: documentId})

	if err != nil {
		log.Fatalf("could not send to: %v", err)
	}

	//response, _ := r.Recv()
	//response2, _ := r.()

	/*log.Printf("r1 " + response.InitialDocumentContent.GetDocumentId())

	for _, user := range response.ConnectedUsers {
		log.Print("r1 " + user.UserId + " " + user.Name + " " + user.Icon)
	}

	log.Printf("r2 " +  response2.InitialDocumentContent.GetDocumentId())
	for _, user := range response2.ConnectedUsers {
		log.Print("r2 " + user.UserId + " " + user.Name + " " + user.Icon)
	} */



	/*if (r.Re != "Hello " + name) {
		t.Errorf("Does not equals")
	}*/

	time.Sleep(time.Hour)
}