package main

import (
	pb "gitlab.com/MatthiasReumann/marky-backend/proto"
)

type DocumentStorage struct {
	documents map[string]pb.Document
}
/* Returns new DocumentStorage
 *
 */
func NewDocumentStorage() *DocumentStorage {
	return &DocumentStorage{documents: make(map[string]pb.Document)}
}

/* Creates a new document if non is existent
 *
 */
func (r *DocumentStorage) GetContentByRoomID(roomID string) (pb.Document, error) {
	return r.documents[roomID], nil
}

/* Updates document name of a document in room with given roomID
 *
 */
func (r *DocumentStorage) UpdateDocumentNameByRoomID(roomID, docName string){
	oldDoc := r.documents[roomID]
	r.documents[roomID] = pb.Document{DocumentId: oldDoc.DocumentId, Content: oldDoc.Content, Name: docName}
}

func (r *DocumentStorage) SetContent(roomID, content string) {
	oldDoc := r.documents[roomID]
	r.documents[roomID] = pb.Document{DocumentId: oldDoc.DocumentId, Content: content, Name: oldDoc.Name}
}
