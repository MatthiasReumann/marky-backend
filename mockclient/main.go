package main

import (
	"context"
	pb "gitlab.com/MatthiasReumann/marky-backend/proto"
	"google.golang.org/grpc"
	"log"
	"time"
)

const (
	address = "localhost:1337"
)

func main() {
	// Set up a connection_establishment.proto to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}

	defer conn.Close()

	//mockClient := pb.NewMockClient(conn)
	mockClient := pb.NewUserConnectionClient(conn)

	log.Printf("worked")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)  // heast das feature is so geil (mach das mal in REST :=))
	defer cancel()

	r, err := mockClient.NewURL(ctx, &pb.NewURLRequest{})

	if err != nil {
		log.Fatalf("could not send to: %v", err)
	}

	log.Printf("Got: %s", r.GetUrl())
}