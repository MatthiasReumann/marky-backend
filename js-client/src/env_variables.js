const server_address = process.env.VUE_APP_BACKEND_ADDRESS;
const server_port = process.env.VUE_APP_BACKEND_PORT;

module.exports = {
    server: {
        address: server_address,
        port: server_port
    }
}