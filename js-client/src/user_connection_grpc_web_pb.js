/**
 * @fileoverview gRPC-Web generated client stub for proto
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');


var document_pb = require('./document_pb.js')

var user_pb = require('./user_pb.js')
const proto = {};
proto.proto = require('./user_connection_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.proto.UserConnectionClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.proto.UserConnectionPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.proto.UserConnectionRequest,
 *   !proto.proto.UserConnectionResponse>}
 */
const methodDescriptor_UserConnection_connect = new grpc.web.MethodDescriptor(
  '/proto.UserConnection/connect',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.proto.UserConnectionRequest,
  proto.proto.UserConnectionResponse,
  /**
   * @param {!proto.proto.UserConnectionRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.proto.UserConnectionResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.proto.UserConnectionRequest,
 *   !proto.proto.UserConnectionResponse>}
 */
const methodInfo_UserConnection_connect = new grpc.web.AbstractClientBase.MethodInfo(
  proto.proto.UserConnectionResponse,
  /**
   * @param {!proto.proto.UserConnectionRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.proto.UserConnectionResponse.deserializeBinary
);


/**
 * @param {!proto.proto.UserConnectionRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.proto.UserConnectionResponse>}
 *     The XHR Node Readable Stream
 */
proto.proto.UserConnectionClient.prototype.connect =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/proto.UserConnection/connect',
      request,
      metadata || {},
      methodDescriptor_UserConnection_connect);
};


/**
 * @param {!proto.proto.UserConnectionRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.proto.UserConnectionResponse>}
 *     The XHR Node Readable Stream
 */
proto.proto.UserConnectionPromiseClient.prototype.connect =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/proto.UserConnection/connect',
      request,
      metadata || {},
      methodDescriptor_UserConnection_connect);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.proto.NewURLRequest,
 *   !proto.proto.NewURLResponse>}
 */
const methodDescriptor_UserConnection_newURL = new grpc.web.MethodDescriptor(
  '/proto.UserConnection/newURL',
  grpc.web.MethodType.UNARY,
  proto.proto.NewURLRequest,
  proto.proto.NewURLResponse,
  /**
   * @param {!proto.proto.NewURLRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.proto.NewURLResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.proto.NewURLRequest,
 *   !proto.proto.NewURLResponse>}
 */
const methodInfo_UserConnection_newURL = new grpc.web.AbstractClientBase.MethodInfo(
  proto.proto.NewURLResponse,
  /**
   * @param {!proto.proto.NewURLRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.proto.NewURLResponse.deserializeBinary
);


/**
 * @param {!proto.proto.NewURLRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.proto.NewURLResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.proto.NewURLResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.proto.UserConnectionClient.prototype.newURL =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/proto.UserConnection/newURL',
      request,
      metadata || {},
      methodDescriptor_UserConnection_newURL,
      callback);
};


/**
 * @param {!proto.proto.NewURLRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.proto.NewURLResponse>}
 *     A native promise that resolves to the response
 */
proto.proto.UserConnectionPromiseClient.prototype.newURL =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/proto.UserConnection/newURL',
      request,
      metadata || {},
      methodDescriptor_UserConnection_newURL);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.proto.UpdateDocumentRequest,
 *   !proto.proto.UpdateDocumentResponse>}
 */
const methodDescriptor_UserConnection_updateDocumentName = new grpc.web.MethodDescriptor(
  '/proto.UserConnection/updateDocumentName',
  grpc.web.MethodType.UNARY,
  proto.proto.UpdateDocumentRequest,
  proto.proto.UpdateDocumentResponse,
  /**
   * @param {!proto.proto.UpdateDocumentRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.proto.UpdateDocumentResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.proto.UpdateDocumentRequest,
 *   !proto.proto.UpdateDocumentResponse>}
 */
const methodInfo_UserConnection_updateDocumentName = new grpc.web.AbstractClientBase.MethodInfo(
  proto.proto.UpdateDocumentResponse,
  /**
   * @param {!proto.proto.UpdateDocumentRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.proto.UpdateDocumentResponse.deserializeBinary
);


/**
 * @param {!proto.proto.UpdateDocumentRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.proto.UpdateDocumentResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.proto.UpdateDocumentResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.proto.UserConnectionClient.prototype.updateDocumentName =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/proto.UserConnection/updateDocumentName',
      request,
      metadata || {},
      methodDescriptor_UserConnection_updateDocumentName,
      callback);
};


/**
 * @param {!proto.proto.UpdateDocumentRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.proto.UpdateDocumentResponse>}
 *     A native promise that resolves to the response
 */
proto.proto.UserConnectionPromiseClient.prototype.updateDocumentName =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/proto.UserConnection/updateDocumentName',
      request,
      metadata || {},
      methodDescriptor_UserConnection_updateDocumentName);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.proto.TextChangedRequest,
 *   !proto.proto.TextChangedResponse>}
 */
const methodDescriptor_UserConnection_textChanged = new grpc.web.MethodDescriptor(
  '/proto.UserConnection/textChanged',
  grpc.web.MethodType.UNARY,
  proto.proto.TextChangedRequest,
  proto.proto.TextChangedResponse,
  /**
   * @param {!proto.proto.TextChangedRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.proto.TextChangedResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.proto.TextChangedRequest,
 *   !proto.proto.TextChangedResponse>}
 */
const methodInfo_UserConnection_textChanged = new grpc.web.AbstractClientBase.MethodInfo(
  proto.proto.TextChangedResponse,
  /**
   * @param {!proto.proto.TextChangedRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.proto.TextChangedResponse.deserializeBinary
);


/**
 * @param {!proto.proto.TextChangedRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.proto.TextChangedResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.proto.TextChangedResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.proto.UserConnectionClient.prototype.textChanged =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/proto.UserConnection/textChanged',
      request,
      metadata || {},
      methodDescriptor_UserConnection_textChanged,
      callback);
};


/**
 * @param {!proto.proto.TextChangedRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.proto.TextChangedResponse>}
 *     A native promise that resolves to the response
 */
proto.proto.UserConnectionPromiseClient.prototype.textChanged =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/proto.UserConnection/textChanged',
      request,
      metadata || {},
      methodDescriptor_UserConnection_textChanged);
};


module.exports = proto.proto;

