/**
 * @fileoverview gRPC-Web generated client stub for proto
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.proto = require('./mock_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.proto.MockClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.proto.MockPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.proto.DoRequest,
 *   !proto.proto.DoReply>}
 */
const methodDescriptor_Mock_DoSomething = new grpc.web.MethodDescriptor(
  '/proto.Mock/DoSomething',
  grpc.web.MethodType.UNARY,
  proto.proto.DoRequest,
  proto.proto.DoReply,
  /**
   * @param {!proto.proto.DoRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.proto.DoReply.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.proto.DoRequest,
 *   !proto.proto.DoReply>}
 */
const methodInfo_Mock_DoSomething = new grpc.web.AbstractClientBase.MethodInfo(
  proto.proto.DoReply,
  /**
   * @param {!proto.proto.DoRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.proto.DoReply.deserializeBinary
);


/**
 * @param {!proto.proto.DoRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.proto.DoReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.proto.DoReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.proto.MockClient.prototype.doSomething =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/proto.Mock/DoSomething',
      request,
      metadata || {},
      methodDescriptor_Mock_DoSomething,
      callback);
};


/**
 * @param {!proto.proto.DoRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.proto.DoReply>}
 *     A native promise that resolves to the response
 */
proto.proto.MockPromiseClient.prototype.doSomething =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/proto.Mock/DoSomething',
      request,
      metadata || {},
      methodDescriptor_Mock_DoSomething);
};


module.exports = proto.proto;

