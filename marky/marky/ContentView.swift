//
//  ContentView.swift
//  marky
//
//  Created by Christoph Schnabl on 25.01.20.
//  Copyright © 2020 Christoph Schnabl. All rights reserved.
//

import SwiftUI
import MarkdownView

struct ContentView: View {

    @State private var documentId : String = "id"
    @State private var documentContent : String = "# hansi"
    
    var body: some View {
        VStack {
            HStack {
                TextField("Enter Document ID", text: $documentId)
                
                Button(action: {
                    self.documentContent = DataRepository.repo.getInitialContentForDocument(documentId: self.documentId)
                }) {
                    Text("Connect")
                }
            }
            MarkView(documentContent: documentContent)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
