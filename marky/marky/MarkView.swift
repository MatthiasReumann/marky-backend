//
//  MarkView.swift
//  marky
//
//  Created by Christoph Schnabl on 26.01.20.
//  Copyright © 2020 Christoph Schnabl. All rights reserved.
//

import SwiftUI
import MarkdownView
import WebKit
  
struct MarkView : UIViewRepresentable {
    let documentContent: String
      
    func makeUIView(context: Context) -> MarkdownView  {
        return MarkdownView()
    }
      
    func updateUIView(_ uiView: MarkdownView, context: Context) {
        uiView.load(markdown: documentContent)
    }
      
}
  

struct MarkView_Previews : PreviewProvider {
    static var previews: some View {
        MarkView(documentContent: "# Preview Text \n ## Header 2")
    }
}

