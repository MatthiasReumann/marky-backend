//
//  DataRepository.swift
//  marky
//
//  Created by Christoph Schnabl on 25.01.20.
//  Copyright © 2020 Christoph Schnabl. All rights reserved.
//

import Foundation
import SwiftGRPC

class DataRepository {
    
    static let repo = DataRepository()
    private init() {}
    private let client = Proto_UserConnectionServiceClient.init(address: "localhost:8080", secure: false)
        
    func getInitialContentForDocument(documentId: String) -> String {
        // let respinse = try? client.newURL(request)
        // print(respinse?.url)
        
        var request = Proto_UserConnectionRequest()
        var user = Proto_User()
        user.userID = "hansi-ios"
        user.icon = "hansi-icon"
        user.name = "name-ios"
        
        request.connectingUser = user
        request.documentID = documentId
            
        let connectionCall = try? client.connect(request, completion: nil)
        
        while true {
            let responseMessage = try? connectionCall!.receive()
            
            // print("Received: \(responseMessage!.initialDocumentContent.content)")
            
            return responseMessage!.initialDocumentContent.content
        }
    }
}
