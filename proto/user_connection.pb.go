// Code generated by protoc-gen-go. DO NOT EDIT.
// source: user_connection.proto

package proto

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type UserConnectionRequest struct {
	ConnectingUser       *User    `protobuf:"bytes,1,opt,name=connecting_user,json=connectingUser,proto3" json:"connecting_user,omitempty"`
	DocumentId           string   `protobuf:"bytes,2,opt,name=document_id,json=documentId,proto3" json:"document_id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UserConnectionRequest) Reset()         { *m = UserConnectionRequest{} }
func (m *UserConnectionRequest) String() string { return proto.CompactTextString(m) }
func (*UserConnectionRequest) ProtoMessage()    {}
func (*UserConnectionRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_b9da96e5816b2a4e, []int{0}
}

func (m *UserConnectionRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UserConnectionRequest.Unmarshal(m, b)
}
func (m *UserConnectionRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UserConnectionRequest.Marshal(b, m, deterministic)
}
func (m *UserConnectionRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UserConnectionRequest.Merge(m, src)
}
func (m *UserConnectionRequest) XXX_Size() int {
	return xxx_messageInfo_UserConnectionRequest.Size(m)
}
func (m *UserConnectionRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_UserConnectionRequest.DiscardUnknown(m)
}

var xxx_messageInfo_UserConnectionRequest proto.InternalMessageInfo

func (m *UserConnectionRequest) GetConnectingUser() *User {
	if m != nil {
		return m.ConnectingUser
	}
	return nil
}

func (m *UserConnectionRequest) GetDocumentId() string {
	if m != nil {
		return m.DocumentId
	}
	return ""
}

type UserConnectionResponse struct {
	InitialDocumentContent *Document `protobuf:"bytes,1,opt,name=initial_document_content,json=initialDocumentContent,proto3" json:"initial_document_content,omitempty"`
	ConnectedUsers         []*User   `protobuf:"bytes,2,rep,name=connected_users,json=connectedUsers,proto3" json:"connected_users,omitempty"`
	XXX_NoUnkeyedLiteral   struct{}  `json:"-"`
	XXX_unrecognized       []byte    `json:"-"`
	XXX_sizecache          int32     `json:"-"`
}

func (m *UserConnectionResponse) Reset()         { *m = UserConnectionResponse{} }
func (m *UserConnectionResponse) String() string { return proto.CompactTextString(m) }
func (*UserConnectionResponse) ProtoMessage()    {}
func (*UserConnectionResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_b9da96e5816b2a4e, []int{1}
}

func (m *UserConnectionResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UserConnectionResponse.Unmarshal(m, b)
}
func (m *UserConnectionResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UserConnectionResponse.Marshal(b, m, deterministic)
}
func (m *UserConnectionResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UserConnectionResponse.Merge(m, src)
}
func (m *UserConnectionResponse) XXX_Size() int {
	return xxx_messageInfo_UserConnectionResponse.Size(m)
}
func (m *UserConnectionResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_UserConnectionResponse.DiscardUnknown(m)
}

var xxx_messageInfo_UserConnectionResponse proto.InternalMessageInfo

func (m *UserConnectionResponse) GetInitialDocumentContent() *Document {
	if m != nil {
		return m.InitialDocumentContent
	}
	return nil
}

func (m *UserConnectionResponse) GetConnectedUsers() []*User {
	if m != nil {
		return m.ConnectedUsers
	}
	return nil
}

type NewURLRequest struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *NewURLRequest) Reset()         { *m = NewURLRequest{} }
func (m *NewURLRequest) String() string { return proto.CompactTextString(m) }
func (*NewURLRequest) ProtoMessage()    {}
func (*NewURLRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_b9da96e5816b2a4e, []int{2}
}

func (m *NewURLRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_NewURLRequest.Unmarshal(m, b)
}
func (m *NewURLRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_NewURLRequest.Marshal(b, m, deterministic)
}
func (m *NewURLRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_NewURLRequest.Merge(m, src)
}
func (m *NewURLRequest) XXX_Size() int {
	return xxx_messageInfo_NewURLRequest.Size(m)
}
func (m *NewURLRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_NewURLRequest.DiscardUnknown(m)
}

var xxx_messageInfo_NewURLRequest proto.InternalMessageInfo

type NewURLResponse struct {
	Url                  string   `protobuf:"bytes,1,opt,name=url,proto3" json:"url,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *NewURLResponse) Reset()         { *m = NewURLResponse{} }
func (m *NewURLResponse) String() string { return proto.CompactTextString(m) }
func (*NewURLResponse) ProtoMessage()    {}
func (*NewURLResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_b9da96e5816b2a4e, []int{3}
}

func (m *NewURLResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_NewURLResponse.Unmarshal(m, b)
}
func (m *NewURLResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_NewURLResponse.Marshal(b, m, deterministic)
}
func (m *NewURLResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_NewURLResponse.Merge(m, src)
}
func (m *NewURLResponse) XXX_Size() int {
	return xxx_messageInfo_NewURLResponse.Size(m)
}
func (m *NewURLResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_NewURLResponse.DiscardUnknown(m)
}

var xxx_messageInfo_NewURLResponse proto.InternalMessageInfo

func (m *NewURLResponse) GetUrl() string {
	if m != nil {
		return m.Url
	}
	return ""
}

type UpdateDocumentRequest struct {
	DocumentID           string   `protobuf:"bytes,1,opt,name=documentID,proto3" json:"documentID,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UpdateDocumentRequest) Reset()         { *m = UpdateDocumentRequest{} }
func (m *UpdateDocumentRequest) String() string { return proto.CompactTextString(m) }
func (*UpdateDocumentRequest) ProtoMessage()    {}
func (*UpdateDocumentRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_b9da96e5816b2a4e, []int{4}
}

func (m *UpdateDocumentRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UpdateDocumentRequest.Unmarshal(m, b)
}
func (m *UpdateDocumentRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UpdateDocumentRequest.Marshal(b, m, deterministic)
}
func (m *UpdateDocumentRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UpdateDocumentRequest.Merge(m, src)
}
func (m *UpdateDocumentRequest) XXX_Size() int {
	return xxx_messageInfo_UpdateDocumentRequest.Size(m)
}
func (m *UpdateDocumentRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_UpdateDocumentRequest.DiscardUnknown(m)
}

var xxx_messageInfo_UpdateDocumentRequest proto.InternalMessageInfo

func (m *UpdateDocumentRequest) GetDocumentID() string {
	if m != nil {
		return m.DocumentID
	}
	return ""
}

func (m *UpdateDocumentRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

type UpdateDocumentResponse struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UpdateDocumentResponse) Reset()         { *m = UpdateDocumentResponse{} }
func (m *UpdateDocumentResponse) String() string { return proto.CompactTextString(m) }
func (*UpdateDocumentResponse) ProtoMessage()    {}
func (*UpdateDocumentResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_b9da96e5816b2a4e, []int{5}
}

func (m *UpdateDocumentResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UpdateDocumentResponse.Unmarshal(m, b)
}
func (m *UpdateDocumentResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UpdateDocumentResponse.Marshal(b, m, deterministic)
}
func (m *UpdateDocumentResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UpdateDocumentResponse.Merge(m, src)
}
func (m *UpdateDocumentResponse) XXX_Size() int {
	return xxx_messageInfo_UpdateDocumentResponse.Size(m)
}
func (m *UpdateDocumentResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_UpdateDocumentResponse.DiscardUnknown(m)
}

var xxx_messageInfo_UpdateDocumentResponse proto.InternalMessageInfo

type TextChangedRequest struct {
	DocumentContent      string   `protobuf:"bytes,1,opt,name=document_content,json=documentContent,proto3" json:"document_content,omitempty"`
	RoomID               string   `protobuf:"bytes,2,opt,name=roomID,proto3" json:"roomID,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *TextChangedRequest) Reset()         { *m = TextChangedRequest{} }
func (m *TextChangedRequest) String() string { return proto.CompactTextString(m) }
func (*TextChangedRequest) ProtoMessage()    {}
func (*TextChangedRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_b9da96e5816b2a4e, []int{6}
}

func (m *TextChangedRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TextChangedRequest.Unmarshal(m, b)
}
func (m *TextChangedRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TextChangedRequest.Marshal(b, m, deterministic)
}
func (m *TextChangedRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TextChangedRequest.Merge(m, src)
}
func (m *TextChangedRequest) XXX_Size() int {
	return xxx_messageInfo_TextChangedRequest.Size(m)
}
func (m *TextChangedRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_TextChangedRequest.DiscardUnknown(m)
}

var xxx_messageInfo_TextChangedRequest proto.InternalMessageInfo

func (m *TextChangedRequest) GetDocumentContent() string {
	if m != nil {
		return m.DocumentContent
	}
	return ""
}

func (m *TextChangedRequest) GetRoomID() string {
	if m != nil {
		return m.RoomID
	}
	return ""
}

type TextChangedResponse struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *TextChangedResponse) Reset()         { *m = TextChangedResponse{} }
func (m *TextChangedResponse) String() string { return proto.CompactTextString(m) }
func (*TextChangedResponse) ProtoMessage()    {}
func (*TextChangedResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_b9da96e5816b2a4e, []int{7}
}

func (m *TextChangedResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TextChangedResponse.Unmarshal(m, b)
}
func (m *TextChangedResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TextChangedResponse.Marshal(b, m, deterministic)
}
func (m *TextChangedResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TextChangedResponse.Merge(m, src)
}
func (m *TextChangedResponse) XXX_Size() int {
	return xxx_messageInfo_TextChangedResponse.Size(m)
}
func (m *TextChangedResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_TextChangedResponse.DiscardUnknown(m)
}

var xxx_messageInfo_TextChangedResponse proto.InternalMessageInfo

func init() {
	proto.RegisterType((*UserConnectionRequest)(nil), "proto.UserConnectionRequest")
	proto.RegisterType((*UserConnectionResponse)(nil), "proto.UserConnectionResponse")
	proto.RegisterType((*NewURLRequest)(nil), "proto.NewURLRequest")
	proto.RegisterType((*NewURLResponse)(nil), "proto.NewURLResponse")
	proto.RegisterType((*UpdateDocumentRequest)(nil), "proto.UpdateDocumentRequest")
	proto.RegisterType((*UpdateDocumentResponse)(nil), "proto.UpdateDocumentResponse")
	proto.RegisterType((*TextChangedRequest)(nil), "proto.TextChangedRequest")
	proto.RegisterType((*TextChangedResponse)(nil), "proto.TextChangedResponse")
}

func init() { proto.RegisterFile("user_connection.proto", fileDescriptor_b9da96e5816b2a4e) }

var fileDescriptor_b9da96e5816b2a4e = []byte{
	// 406 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x74, 0x92, 0xdf, 0x8e, 0x9a, 0x40,
	0x14, 0xc6, 0x45, 0x5b, 0x1b, 0x0f, 0x29, 0x98, 0x69, 0x21, 0x94, 0xf4, 0x8f, 0x99, 0x2b, 0x7b,
	0x63, 0x1a, 0xdb, 0xa4, 0x0f, 0xa0, 0x69, 0x62, 0xdb, 0x78, 0x41, 0x6b, 0xf6, 0x92, 0xb0, 0xcc,
	0x89, 0x4b, 0x22, 0x33, 0x2e, 0x0c, 0xd9, 0x7d, 0x95, 0x7d, 0x8d, 0x7d, 0xc2, 0x0d, 0x30, 0x23,
	0x82, 0x78, 0xe5, 0xcc, 0x39, 0xe7, 0x3b, 0xdf, 0xc7, 0x6f, 0x04, 0xa7, 0xc8, 0x31, 0x0b, 0x63,
	0xc1, 0x39, 0xc6, 0x32, 0x11, 0x7c, 0x71, 0xcc, 0x84, 0x14, 0xe4, 0x75, 0xf5, 0xe3, 0x5b, 0x4c,
	0xc4, 0x45, 0x8a, 0x5c, 0xd6, 0x65, 0x1f, 0xca, 0xe9, 0xfa, 0x4c, 0x39, 0x38, 0xbb, 0x1c, 0xb3,
	0xd5, 0x49, 0x1a, 0xe0, 0x7d, 0x81, 0xb9, 0x24, 0x3f, 0xc0, 0xd6, 0xfb, 0xf8, 0x3e, 0x2c, 0x15,
	0x9e, 0x31, 0x33, 0xe6, 0xe6, 0xd2, 0xac, 0x95, 0x8b, 0x52, 0x16, 0x58, 0xcd, 0x4c, 0x79, 0x27,
	0x5f, 0xc0, 0xd4, 0x66, 0x61, 0xc2, 0xbc, 0xe1, 0xcc, 0x98, 0x4f, 0x02, 0xd0, 0xa5, 0x0d, 0xa3,
	0x4f, 0x06, 0xb8, 0x5d, 0xc3, 0xfc, 0x28, 0x78, 0x8e, 0x64, 0x03, 0x5e, 0xc2, 0x13, 0x99, 0x44,
	0x87, 0xf0, 0xb4, 0x23, 0x16, 0x5c, 0x22, 0x97, 0xca, 0xda, 0x56, 0xd6, 0x6b, 0xd5, 0x0e, 0x5c,
	0x25, 0xd0, 0x85, 0x55, 0x3d, 0x7e, 0x16, 0x1e, 0x59, 0x95, 0x3d, 0xf7, 0x86, 0xb3, 0xd1, 0xb5,
	0xf0, 0xc8, 0xca, 0x6b, 0x4e, 0x6d, 0x78, 0xbb, 0xc5, 0x87, 0x5d, 0xf0, 0x57, 0x31, 0xa0, 0x14,
	0x2c, 0x5d, 0x50, 0x19, 0xa7, 0x30, 0x2a, 0xb2, 0x43, 0x15, 0x67, 0x12, 0x94, 0x47, 0xfa, 0x07,
	0x9c, 0xdd, 0x91, 0x45, 0x12, 0x4f, 0xa1, 0x14, 0xc0, 0xcf, 0xd0, 0x7c, 0xf7, 0x5a, 0x29, 0xce,
	0x2a, 0x84, 0xc0, 0x2b, 0x1e, 0xa5, 0xa8, 0x18, 0x55, 0x67, 0xea, 0x81, 0xdb, 0x5d, 0x56, 0x1b,
	0xd3, 0x1b, 0x20, 0xff, 0xf1, 0x51, 0xae, 0xee, 0x22, 0xbe, 0x47, 0xa6, 0x3d, 0xbe, 0xc2, 0xb4,
	0x17, 0xd5, 0x24, 0xb0, 0x59, 0x07, 0x89, 0x0b, 0xe3, 0x4c, 0x88, 0x74, 0xb3, 0x56, 0x86, 0xea,
	0x46, 0x1d, 0x78, 0xd7, 0x5a, 0x5c, 0xfb, 0x2d, 0x9f, 0x87, 0x60, 0xb5, 0xdf, 0x89, 0xfc, 0x86,
	0x37, 0x0a, 0x18, 0xf9, 0x78, 0x86, 0xf1, 0xe2, 0xaf, 0xe3, 0x7f, 0xba, 0xd2, 0x55, 0x9f, 0x32,
	0xf8, 0x66, 0x90, 0x9f, 0x30, 0xe6, 0x15, 0x59, 0xf2, 0x5e, 0x0d, 0xb7, 0xc8, 0xfb, 0x4e, 0xa7,
	0xaa, 0xa5, 0xe4, 0x1f, 0x90, 0xa2, 0x45, 0x68, 0x1b, 0xa5, 0xd8, 0xe4, 0xe9, 0x7b, 0x89, 0x26,
	0x4f, 0x3f, 0xda, 0x01, 0xf9, 0x05, 0xa6, 0x6c, 0x18, 0x90, 0x0f, 0x6a, 0xfe, 0x12, 0xb8, 0xef,
	0xf7, 0xb5, 0xf4, 0x9e, 0xdb, 0x71, 0xd5, 0xfc, 0xfe, 0x12, 0x00, 0x00, 0xff, 0xff, 0x91, 0x3d,
	0x00, 0x17, 0x8f, 0x03, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// UserConnectionClient is the client API for UserConnection service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type UserConnectionClient interface {
	Connect(ctx context.Context, in *UserConnectionRequest, opts ...grpc.CallOption) (UserConnection_ConnectClient, error)
	NewURL(ctx context.Context, in *NewURLRequest, opts ...grpc.CallOption) (*NewURLResponse, error)
	UpdateDocumentName(ctx context.Context, in *UpdateDocumentRequest, opts ...grpc.CallOption) (*UpdateDocumentResponse, error)
	TextChanged(ctx context.Context, in *TextChangedRequest, opts ...grpc.CallOption) (*TextChangedResponse, error)
}

type userConnectionClient struct {
	cc *grpc.ClientConn
}

func NewUserConnectionClient(cc *grpc.ClientConn) UserConnectionClient {
	return &userConnectionClient{cc}
}

func (c *userConnectionClient) Connect(ctx context.Context, in *UserConnectionRequest, opts ...grpc.CallOption) (UserConnection_ConnectClient, error) {
	stream, err := c.cc.NewStream(ctx, &_UserConnection_serviceDesc.Streams[0], "/proto.UserConnection/connect", opts...)
	if err != nil {
		return nil, err
	}
	x := &userConnectionConnectClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type UserConnection_ConnectClient interface {
	Recv() (*UserConnectionResponse, error)
	grpc.ClientStream
}

type userConnectionConnectClient struct {
	grpc.ClientStream
}

func (x *userConnectionConnectClient) Recv() (*UserConnectionResponse, error) {
	m := new(UserConnectionResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *userConnectionClient) NewURL(ctx context.Context, in *NewURLRequest, opts ...grpc.CallOption) (*NewURLResponse, error) {
	out := new(NewURLResponse)
	err := c.cc.Invoke(ctx, "/proto.UserConnection/newURL", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userConnectionClient) UpdateDocumentName(ctx context.Context, in *UpdateDocumentRequest, opts ...grpc.CallOption) (*UpdateDocumentResponse, error) {
	out := new(UpdateDocumentResponse)
	err := c.cc.Invoke(ctx, "/proto.UserConnection/updateDocumentName", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userConnectionClient) TextChanged(ctx context.Context, in *TextChangedRequest, opts ...grpc.CallOption) (*TextChangedResponse, error) {
	out := new(TextChangedResponse)
	err := c.cc.Invoke(ctx, "/proto.UserConnection/textChanged", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// UserConnectionServer is the server API for UserConnection service.
type UserConnectionServer interface {
	Connect(*UserConnectionRequest, UserConnection_ConnectServer) error
	NewURL(context.Context, *NewURLRequest) (*NewURLResponse, error)
	UpdateDocumentName(context.Context, *UpdateDocumentRequest) (*UpdateDocumentResponse, error)
	TextChanged(context.Context, *TextChangedRequest) (*TextChangedResponse, error)
}

// UnimplementedUserConnectionServer can be embedded to have forward compatible implementations.
type UnimplementedUserConnectionServer struct {
}

func (*UnimplementedUserConnectionServer) Connect(req *UserConnectionRequest, srv UserConnection_ConnectServer) error {
	return status.Errorf(codes.Unimplemented, "method Connect not implemented")
}
func (*UnimplementedUserConnectionServer) NewURL(ctx context.Context, req *NewURLRequest) (*NewURLResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method NewURL not implemented")
}
func (*UnimplementedUserConnectionServer) UpdateDocumentName(ctx context.Context, req *UpdateDocumentRequest) (*UpdateDocumentResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateDocumentName not implemented")
}
func (*UnimplementedUserConnectionServer) TextChanged(ctx context.Context, req *TextChangedRequest) (*TextChangedResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method TextChanged not implemented")
}

func RegisterUserConnectionServer(s *grpc.Server, srv UserConnectionServer) {
	s.RegisterService(&_UserConnection_serviceDesc, srv)
}

func _UserConnection_Connect_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(UserConnectionRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(UserConnectionServer).Connect(m, &userConnectionConnectServer{stream})
}

type UserConnection_ConnectServer interface {
	Send(*UserConnectionResponse) error
	grpc.ServerStream
}

type userConnectionConnectServer struct {
	grpc.ServerStream
}

func (x *userConnectionConnectServer) Send(m *UserConnectionResponse) error {
	return x.ServerStream.SendMsg(m)
}

func _UserConnection_NewURL_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(NewURLRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserConnectionServer).NewURL(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.UserConnection/NewURL",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserConnectionServer).NewURL(ctx, req.(*NewURLRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserConnection_UpdateDocumentName_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateDocumentRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserConnectionServer).UpdateDocumentName(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.UserConnection/UpdateDocumentName",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserConnectionServer).UpdateDocumentName(ctx, req.(*UpdateDocumentRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserConnection_TextChanged_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(TextChangedRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserConnectionServer).TextChanged(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.UserConnection/TextChanged",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserConnectionServer).TextChanged(ctx, req.(*TextChangedRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _UserConnection_serviceDesc = grpc.ServiceDesc{
	ServiceName: "proto.UserConnection",
	HandlerType: (*UserConnectionServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "newURL",
			Handler:    _UserConnection_NewURL_Handler,
		},
		{
			MethodName: "updateDocumentName",
			Handler:    _UserConnection_UpdateDocumentName_Handler,
		},
		{
			MethodName: "textChanged",
			Handler:    _UserConnection_TextChanged_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "connect",
			Handler:       _UserConnection_Connect_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "user_connection.proto",
}
